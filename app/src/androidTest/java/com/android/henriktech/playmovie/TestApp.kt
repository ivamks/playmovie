
package com.android.henriktech.playmovie

import android.app.Application

/**
 * We use a separate App for tests to prevent initializing dependency injection.
 *
 * See [com.android.henriktech.playmovie.util.TmdbTestRunner].
 */
class TestApp : Application()
