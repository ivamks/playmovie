
package com.android.henriktech.playmovie.ui.common

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
