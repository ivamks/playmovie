
package com.android.henriktech.playmovie.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
